# Scope of SDV Working Group

> **Status: DRAFT**
>
> Purpose of this document is to verbalize the intended scope of the Eclipse SDV working group. Currently, the document reflects input that was provided to the Eclipse Foundation on what the scope of this working group might be. Raw documentation of this input can be found in [this mind map](input/SDV Scope Mind Map.pdf).
>
> Any change to this document (beyond documenting the raw input) should be made via pull requests to allow the community to have a discussion around proposed changes.

## What

Collected topic areas for targeting SDV group activities include:

* Technically focussed:
  * Development of abstractions, in combination with:
    * desire for well-defined interfaces and definition of standards / specifications (referenced from a general modularity goal)
  * The group shall develop open source code,
    * including adopting suitable containerization frameworks
  * General acknowledgement of safety and security goals
* Non-technical activities are also in focus:
  * Community building
    * both in tech and automotive spaces
  * Brand building and -promotion

## Where

Originally, the community provided three main areas of where artefacts of this working group would be active:

* Edge / Onboard, including topics like
  * in-vehicle software stacks and their runtime environment
  * an uniform vehicle data model
  * an uniform communication model
  * an uniform vehicle service model
  * a capability oriented architecture
  * an event fabric
* Software operations related topic, like
  * controlling of software deployments
  * capabilities that allow for monitoring of software artefacts and their environment
* Development related topics, like
  * highly integrated toolchains that allow to work 'from requirement to car/deployment'
  * integration across supply chain

During a first discussion, it was concluded that sticking to this structure might not be helpful, as it is aligning along existing structures which might not be applicable for truly Software-defined vehicles. Nevertheless, the mentioned topics are more than valid. It was discussed to structure the work along certain areas/challenges/features/solutions, such as Over-the-air updates or connectivity related topics (ie, Vehicle-to-X).

> TODO: translate the above structure into a more area/challenge/feature/solution driven structure.

## Current Environment

Group input about the environment we find ourselves in is highlighting two aspects:
* Prevalent product and market approach
  * fragmentation and multitudes of components/products
  * tendency for vendor lock-in and vendor-specific solutions
* Existing landscape of related and relevant other projects/groups/ecosystems
  * AUTOSAR (adaptive and classic flavors)
  * SOAFEE initiative
  * COVESA/genivi
* In general, we face a set of different technological scopes like
  * General-Purpose Operating Systems (GP-OS)
  * Real-Time Operating Systems (RT-OS), with explicit safety implications
  * Cloud-based Car solutions

## How





